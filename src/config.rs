use num_traits::Zero;
use serde::export::fmt::Display;
use serde_derive::Deserialize;
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::ops::RangeInclusive;
use std::path::Path;

#[derive(Deserialize)]
pub(crate) struct Config {
    pub(crate) collider_cell_width: f64,
    pub(crate) collider_padding: f64,
    pub(crate) screen_width: u32,
    pub(crate) screen_height: u32,
    pub(crate) fuel_purity: f64,
    pub(crate) pellet_distance: f64,
    pub(crate) pellet_columns: usize,
    pub(crate) pellet_rows: usize,
    pub(crate) implosion_speed: f64,
    pub(crate) wall_thickness: f64,
    pub(crate) simulation_time_step: f64,
    pub(crate) particle_diameter_factor: f64,
    pub(crate) fuel_mass: f64,
    pub(crate) ballast_initial_mass: f64,
    pub(crate) neutron_mass: f64,
    pub(crate) neutron_energy_coefficient: f64,
    pub(crate) fission_energy: f64,
    pub(crate) neutrons_per_fission: usize,
    pub(crate) products_per_fission: usize,
    pub(crate) max_neutrons_absorbed: usize,
    pub(crate) init_neutrons: usize,
    pub(crate) init_neutron_energy: f64,
}

pub(crate) fn read_config(config_path: &Path) -> Result<Config, String> {
    fn read(config_path: &Path) -> Result<Config, Box<dyn Error>> {
        let mut config_file = File::open(config_path)?;
        let mut config_string = String::new();
        config_file.read_to_string(&mut config_string)?;
        let config: Config = toml::from_str(&config_string)?;
        verify_config(&config)?;
        Ok(config)
    }

    read(config_path).map_err(|error| format!("{}: {}", config_path.to_string_lossy(), error))
}

fn verify_config(config: &Config) -> Result<(), String> {
    check(&config.fuel_purity, "fuel_purity", IsInRange(0.0..=1.0))?;
    check(&config.pellet_distance, "pellet_distance", Positive)?;
    check(&config.implosion_speed, "implosion_speed", Positive)?;
    check(&config.wall_thickness, "wall_thickness", GreatOrEqual(1.0))?;
    check(
        &config.simulation_time_step,
        "simulation_time_step",
        Positive,
    )?;
    check(
        &config.particle_diameter_factor,
        "particle_diameter_factor",
        Positive,
    )?;
    check(&config.fuel_mass, "fuel_mass", Positive)?;
    check(
        &config.ballast_initial_mass,
        "ballast_initial_mass",
        Positive,
    )?;
    check(&config.neutron_mass, "neutron_mass", Positive)?;
    check(
        &config.neutron_energy_coefficient,
        "neutron_energy_coefficient",
        IsInRange(0.0..=1.0),
    )?;
    check(&config.fission_energy, "fission_energy", Positive)?;
    check(
        &config.neutrons_per_fission,
        "neutrons_per_fission",
        Positive,
    )?;
    check(
        &config.products_per_fission,
        "products_per_fission",
        Positive,
    )?;
    check(&config.init_neutrons, "init_neutrons", Positive)?;
    check(&config.init_neutron_energy, "init_neutron_energy", Positive)?;
    Ok(())
}

fn check<T, R: Rule<T>>(value: &T, name: &'static str, rule: R) -> Result<(), String> {
    if rule.condition(value) {
        Ok(())
    } else {
        Err(rule.error_message(value, name))
    }
}

struct GreatOrEqual<T>(T);
struct Positive;
struct IsInRange<T>(RangeInclusive<T>);

trait Rule<T> {
    fn condition(&self, value: &T) -> bool;
    fn error_message(&self, value: &T, name: &'static str) -> String;
}

impl<T: PartialOrd + Display> Rule<T> for GreatOrEqual<T> {
    fn condition(&self, value: &T) -> bool {
        value >= &self.0
    }

    fn error_message(&self, value: &T, name: &'static str) -> String {
        format!("{} ({}) must be greater than {}", name, value, &self.0)
    }
}

impl<T: Zero + PartialOrd + Display> Rule<T> for Positive {
    fn condition(&self, value: &T) -> bool {
        value > &Zero::zero()
    }

    fn error_message(&self, value: &T, name: &'static str) -> String {
        format!("{} ({}) must be positive", name, value)
    }
}

impl<T: PartialOrd + Display> Rule<T> for IsInRange<T> {
    fn condition(&self, value: &T) -> bool {
        self.0.contains(value)
    }

    fn error_message(&self, value: &T, name: &'static str) -> String {
        format!(
            "{} ({}) is out of range [{}; {}]",
            name,
            value,
            self.0.start(),
            self.0.end()
        )
    }
}
