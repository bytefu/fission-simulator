use getopts::Options;
use std::env::args;

pub(crate) struct CommandLine {
    pub(crate) config_path: String,
}

pub(crate) fn parse_command_line() -> (String, Result<CommandLine, (String, i32)>) {
    static HELP_OPTION: &str = "h";
    static CONFIG_OPTION: &str = "c";

    let mut spec = Options::new();
    spec.optflag(HELP_OPTION, "help", "print this help menu");
    spec.optopt(
        CONFIG_OPTION,
        "",
        "simulation configuration file",
        "<CONFIG>",
    );

    let args = args().collect::<Vec<_>>();
    let program_name = &args[0];
    let result = match spec.parse(&args[1..]) {
        Ok(matches) => match matches.opt_str(CONFIG_OPTION).as_ref() {
            Some(config_path) if !matches.opt_present(HELP_OPTION) && matches.free.is_empty() => {
                Ok(CommandLine {
                    config_path: config_path.clone(),
                })
            }

            _ => {
                let brief = format!("Usage: {} -c <CONFIG>", program_name);
                let exit_code = if matches.opt_present(HELP_OPTION) {
                    0
                } else {
                    1
                };
                Err((spec.usage(&brief), exit_code))
            }
        },

        Err(error) => Err((error.to_string(), 1)),
    };
    (program_name.clone(), result)
}
