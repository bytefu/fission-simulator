#![warn(rust_2018_idioms)]

mod cmdline;
mod config;

use crate::cmdline::parse_command_line;
use crate::config::read_config;
use crate::config::Config;
use collider::geom::v2;
use collider::geom::Shape;
use collider::geom::Vec2;
use collider::Collider;
use collider::HbEvent;
use collider::HbId;
use collider::HbProfile;
use enum_iterator::IntoEnumIterator;
use rand::{thread_rng, Rng};
use rayon::prelude::*;
use sdl2::event::Event;
use sdl2::gfx::primitives::DrawRenderer;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Texture;
use static_assertions::const_assert;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::convert::TryFrom;
use std::mem::replace;
use std::ops::Index;
use std::path::Path;
use std::process::exit;
use std::time::Instant;

enum ColorMode {
    ParticleType,
    Energy,
}

fn main() {
    let (program_name, command_line) = parse_command_line();
    let command_line = command_line.unwrap_or_else(|(error, exit_code)| {
        println!("{}: {}", program_name, error);
        exit(exit_code);
    });
    let config = read_config(Path::new(&command_line.config_path)).unwrap_or_else(|error| {
        println!("{}", error);
        exit(1);
    });
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let title = format!("{} v{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
    let window = video_subsystem
        .window(&title, config.screen_width, config.screen_height)
        .position(1000, 0)
        .build()
        .unwrap();
    let mut canvas = window.into_canvas().accelerated().build().unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let ttf_context = sdl2::ttf::init().unwrap();
    let font = ttf_context.load_font("DejaVuSans.ttf", 20).unwrap();
    let texture_creator = canvas.texture_creator();
    let mut stats_text: Option<(Texture<'_>, Rect)> = None;
    let mut reactor = Reactor::new(&config);
    reactor.add_wall(
        v2(
            0.0,
            -f64::from(config.screen_height) / 2.0 + config.wall_thickness / 2.0,
        ),
        v2(f64::from(config.screen_width), config.wall_thickness),
        WallKind::Top,
    );
    reactor.add_wall(
        v2(
            0.0,
            f64::from(config.screen_height) / 2.0 - config.wall_thickness / 2.0,
        ),
        v2(f64::from(config.screen_width), config.wall_thickness),
        WallKind::Bottom,
    );
    reactor.add_wall(
        v2(
            -f64::from(config.screen_width) / 2.0 + config.wall_thickness / 2.0,
            0.0,
        ),
        v2(config.wall_thickness, f64::from(config.screen_height)),
        WallKind::Left,
    );
    reactor.add_wall(
        v2(
            f64::from(config.screen_width) / 2.0 - config.wall_thickness / 2.0,
            0.0,
        ),
        v2(config.wall_thickness, f64::from(config.screen_height)),
        WallKind::Right,
    );
    for wall in WallKind::into_enum_iter() {
        assert!(reactor.wall_kind_to_id.contains_key(&wall));
    }

    const INITIAL_VELOCITY: Vec2 = Vec2 { x: 0.0, y: 0.0 };
    for i in 0..config.pellet_columns {
        for j in 0..config.pellet_rows {
            let x_offset = if j & 1 == 0 {
                0.0
            } else {
                config.pellet_distance / 2.0
            };
            let position = v2(
                -f64::from(config.screen_width) / 2.0
                    + 30.0
                    + config.pellet_distance * (i as f64)
                    + x_offset,
                -f64::from(config.screen_height) / 2.0 + 30.0 + config.pellet_distance * (j as f64),
            );
            if thread_rng().gen_bool(config.fuel_purity) {
                reactor.add_fuel(position, INITIAL_VELOCITY);
            } else {
                reactor.add_ballast(position, INITIAL_VELOCITY, config.ballast_initial_mass);
            }
        }
    }

    let mut last_measure_time = Instant::now();
    let mut frame_count_queue = VecDeque::new();
    let mut fission_count_queue = VecDeque::new();
    let mut frame_count = 0usize;
    let mut max_fissions_per_second = 0.0f64;
    let mut frames_per_second = 0;
    let mut color_mode = ColorMode::ParticleType;

    let time_start = Instant::now();
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,

                Event::KeyDown {
                    keycode: Some(Keycode::I),
                    ..
                } => {
                    const INIT_POSITION: Vec2 = Vec2 { x: 0.0, y: 0.0 };
                    let neutrons = split(
                        config.init_neutrons,
                        config.init_neutron_energy * config.init_neutrons as f64,
                        config.neutron_mass * config.init_neutrons as f64,
                    );
                    for neutron in neutrons {
                        reactor.add_neutron(INIT_POSITION, neutron.velocity);
                    }
                }

                Event::KeyDown {
                    keycode: Some(Keycode::Backslash),
                    ..
                } => {
                    for (&id, &wall) in &reactor.wall_id_to_kind {
                        let mut velocity = reactor.collider.get_hitbox(id).vel;
                        velocity.value += match wall {
                            WallKind::Left => v2(config.implosion_speed, 0.0),
                            WallKind::Right => v2(-config.implosion_speed, 0.0),
                            WallKind::Top => v2(0.0, config.implosion_speed),
                            WallKind::Bottom => v2(0.0, -config.implosion_speed),
                        };
                        velocity.resize += match wall {
                            WallKind::Left => v2(0.0, -config.implosion_speed * 2.0),
                            WallKind::Right => v2(0.0, -config.implosion_speed * 2.0),
                            WallKind::Top => v2(-config.implosion_speed * 2.0, 0.0),
                            WallKind::Bottom => v2(-config.implosion_speed * 2.0, 0.0),
                        };
                        reactor.collider.set_hitbox_vel(id, velocity);
                    }
                }

                Event::KeyDown {
                    keycode: Some(Keycode::Backspace),
                    ..
                } => {
                    for &id in reactor.wall_id_to_kind.keys() {
                        let mut velocity = reactor.collider.get_hitbox(id).vel;
                        velocity.value = v2(0.0, 0.0);
                        velocity.resize = v2(0.0, 0.0);
                        reactor.collider.set_hitbox_vel(id, velocity);
                    }
                }

                Event::KeyDown {
                    keycode: Some(Keycode::N),
                    ..
                } => {
                    color_mode = ColorMode::ParticleType;
                }

                Event::KeyDown {
                    keycode: Some(Keycode::E),
                    ..
                } => {
                    color_mode = ColorMode::Energy;
                }

                _ => {}
            }
        }

        let time_elapsed = reactor.update();

        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();

        let energies = if let ColorMode::Energy = color_mode {
            reactor
                .particles
                .par_iter()
                .map(|(id, state)| {
                    let hitbox = reactor.collider.get_hitbox(*id);
                    let velocity_vector = hitbox.vel.value * time_elapsed;
                    let velocity = (velocity_vector.x * velocity_vector.x
                        + velocity_vector.y * velocity_vector.y)
                        .sqrt();
                    let energy = state.mass * velocity * velocity / 2.0;
                    (*id, energy)
                })
                .collect()
        } else {
            HashMap::new()
        };

        let (_, energy_min, energy_max) = if let ColorMode::Energy = color_mode {
            energies
                .iter()
                .map(|(id, energy)| (*id, *energy, *energy))
                .fold(
                    (0, 0.0f64, 0.000000000000001f64),
                    |(_, energy_min, energy_max), (id, energy, _)| {
                        let (energy_min, energy_max) =
                            (energy_min.min(energy), energy_max.max(energy));
                        (id, energy_min.min(energy), energy_max.max(energy))
                    },
                )
        } else {
            (0, 0.0, 0.000000000001)
        };

        for (id, state) in reactor.particles.iter() {
            let hitbox = reactor.collider.get_hitbox(*id);
            let position = hitbox.value.pos + hitbox.vel.value * time_elapsed;
            let color = match color_mode {
                ColorMode::ParticleType => match &state.kind {
                    ParticleKind::Neutron => Color::RGB(255, 255, 100),
                    ParticleKind::Fuel => Color::RGB(255, 100, 100),
                    ParticleKind::Ballast => Color::RGB(100, 100, 255),
                },

                ColorMode::Energy => {
                    let energy = energies[id];
                    let relative_energy = ((energy - energy_min) / energy_max).powf(0.2);
                    energy_rgb_gradient(relative_energy)
                }
            };
            canvas
                .pixel(
                    i16::try_from(config.screen_width / 2).unwrap() + position.x as i16,
                    i16::try_from(config.screen_height / 2).unwrap() + position.y as i16,
                    color,
                )
                .unwrap();
        }

        for id in reactor.wall_id_to_kind.keys() {
            let hitbox = reactor.collider.get_hitbox(*id);
            let position = hitbox.value.pos + hitbox.vel.value * time_elapsed;
            let size = hitbox.value.dims() + hitbox.vel.resize * time_elapsed;
            let rect = Rect::new(
                i32::try_from(config.screen_width).unwrap() / 2
                    + (position.x - size.x / 2.0) as i32,
                i32::try_from(config.screen_height).unwrap() / 2
                    + (position.y - size.y / 2.0) as i32,
                size.x as u32,
                size.y as u32,
            );
            canvas.set_draw_color(Color::RGB(100, 100, 100));
            canvas.fill_rect(rect).unwrap();
        }

        const FRAMES_MEASURE_PERIOD_MILLIS: u128 = 10;
        const_assert!(FRAMES_MEASURE_PERIOD_MILLIS > 0);

        let real_time = Instant::now();
        let simulation_time = reactor.collider.time();
        if (real_time - last_measure_time).as_millis() >= FRAMES_MEASURE_PERIOD_MILLIS {
            frame_count_queue.push_back((real_time, frame_count));
            fission_count_queue.push_back((simulation_time, reactor.fission_count));
            last_measure_time = real_time;
        }

        let mut updated_stats = false;

        if let Some((measure_start, start_fission_count)) = fission_count_queue.front().cloned() {
            const FISSIONS_AVERAGING_PERIOD: f64 = 0.1;
            const_assert!(FISSIONS_AVERAGING_PERIOD > 0.0);

            let period = simulation_time - measure_start;
            if period > FISSIONS_AVERAGING_PERIOD {
                while let Some((measure_start, _)) = fission_count_queue.front().cloned() {
                    if simulation_time - measure_start > FISSIONS_AVERAGING_PERIOD {
                        fission_count_queue.pop_front();
                    } else {
                        break;
                    }
                }
            }

            let fissions_in_period = reactor.fission_count - start_fission_count;
            let fissions_per_second = fissions_in_period as f64 / period;
            max_fissions_per_second = max_fissions_per_second.max(fissions_per_second);
            updated_stats = true;
        }

        if let Some((measure_start, start_frame_count)) = frame_count_queue.front().cloned() {
            const STATS_AVERAGING_PERIOD_MILLIS: u128 = 100;
            const_assert!(STATS_AVERAGING_PERIOD_MILLIS > 0);

            let period_millis = (real_time - measure_start).as_millis();
            if period_millis > FRAMES_MEASURE_PERIOD_MILLIS {
                while let Some((measure_start, _)) = frame_count_queue.front().cloned() {
                    if (real_time - measure_start).as_millis() > STATS_AVERAGING_PERIOD_MILLIS {
                        frame_count_queue.pop_front();
                    } else {
                        break;
                    }
                }

                let frames_in_period = frame_count - start_frame_count;
                frames_per_second =
                    u128::try_from(frames_in_period).unwrap() * 1000 / period_millis;
                updated_stats = true;
            }
        }

        if updated_stats {
            let mut stats_string = format!("FPS: {}", frames_per_second);

            stats_string.push_str(&format!("\nMax fissions/s: {:.0}", max_fissions_per_second));

            let real_time_elapsed = (real_time - time_start).as_millis() as f64 / 1000.0;
            stats_string.push_str(&format!(
                "\nReal time: {:.1}s\nSimulation time: {:.3}s",
                real_time_elapsed, simulation_time
            ));

            stats_string.push_str("\nParticles:");
            let particle_counts = {
                let mut counts = HashMap::new();
                for kind in ParticleKind::into_enum_iter() {
                    counts.insert(kind, 0usize);
                }
                for particle in reactor.particles.values() {
                    *counts.get_mut(&particle.kind).unwrap() += 1;
                }
                let mut counts = counts.into_iter().collect::<Vec<_>>();
                counts.sort_by_key(|(kind, _)| *kind);
                counts
            };
            for (kind, count) in particle_counts {
                stats_string.push_str(&format!("\n  {:?}: {}", kind, count));
            }

            let rendered = font
                .render(&stats_string)
                .blended_wrapped(Color::RGB(255, 255, 255), config.screen_width)
                .unwrap();
            let rect = rendered.rect();
            let texture = texture_creator
                .create_texture_from_surface(&rendered)
                .unwrap();
            stats_text = Some((texture, rect));
        }

        if let Some((stats_text, stats_text_rect)) = &stats_text {
            canvas
                .copy(stats_text, *stats_text_rect, *stats_text_rect)
                .unwrap();
        }

        canvas.present();
        frame_count += 1;
    }
}

fn energy_rgb_gradient(energy: f64) -> Color {
    let w = (1.0 - energy) * (781.0 - 380.0) + 380.0;
    let (r, g, b) = if w >= 380.0 && w < 440.0 {
        (-(w - 440.0) / (440.0 - 380.0), 0.0, 1.0)
    } else if w >= 440.0 && w < 490.0 {
        (0.0, (w - 440.0) / (490.0 - 440.0), 1.0)
    } else if w >= 490.0 && w < 510.0 {
        (0.0, 1.0, -(w - 510.0) / (510.0 - 490.0))
    } else if w >= 510.0 && w < 580.0 {
        ((w - 510.0) / (580.0 - 510.0), 1.0, 0.0)
    } else if w >= 580.0 && w < 645.0 {
        (1.0, -(w - 645.0) / (645.0 - 580.0), 0.0)
    } else if w >= 645.0 && w < 781.0 {
        (1.0, 0.0, 0.0)
    } else {
        (0.0, 0.0, 0.0)
    };

    // Let the intensity fall off near the vision limits
    let factor = if w >= 380.0 && w < 420.0 {
        0.3 + 0.7 * (w - 380.0) / (420.0 - 380.0)
    } else if w >= 420.0 && w < 701.0 {
        1.0
    } else if w >= 701.0 && w < 781.0 {
        0.3 + 0.7 * (780.0 - w) / (780.0 - 700.0)
    } else {
        0.0
    };

    let gamma = 0.80;
    let r = if r > 0.0 {
        255.0 * (r * factor).powf(gamma)
    } else {
        0.0
    };
    let g = if g > 0.0 {
        255.0 * (g * factor).powf(gamma)
    } else {
        0.0
    };
    let b = if b > 0.0 {
        255.0 * (b * factor).powf(gamma)
    } else {
        0.0
    };
    Color::RGB(r as u8, g as u8, b as u8)
}

struct Reactor<'a> {
    config: &'a Config,
    previous_time: Instant,
    step_count: usize,
    collider: Collider<EntityProfile>,
    last_collider_time: f64,
    next_id: HbId,
    particles: HashMap<HbId, Particle>, // TODO Vec
    wall_id_to_kind: HashMap<HbId, WallKind>,
    wall_kind_to_id: HashMap<WallKind, HbId>,
    fission_count: usize,
}

impl<'a> Reactor<'a> {
    fn new(config: &'a Config) -> Self {
        Self {
            config,
            previous_time: Instant::now(),
            step_count: 0,
            collider: Collider::new(config.collider_cell_width, config.collider_padding),
            last_collider_time: 0.0,
            next_id: 0,
            particles: HashMap::new(),
            wall_id_to_kind: HashMap::new(),
            wall_kind_to_id: HashMap::new(),
            fission_count: 0,
        }
    }

    fn add_wall(&mut self, position: Vec2, size: Vec2, kind: WallKind) {
        let wall_id = self.next_id();
        let wall_profile = EntityProfile::new(wall_id);
        let wall_hitbox = Shape::rect(size).place(position).still();
        self.collider.add_hitbox(wall_profile, wall_hitbox);
        self.wall_id_to_kind.insert(wall_id, kind);
        self.wall_kind_to_id.insert(kind, wall_id);
    }

    fn next_id(&mut self) -> HbId {
        let next_id = self.next_id + 1;
        replace(&mut self.next_id, next_id)
    }

    #[must_use]
    fn update(&mut self) -> f64 {
        let now = Instant::now();
        let elapsed_seconds = (now - self.previous_time).as_nanos() as f64 / 1_000_000_000.0;
        let collider_time = self.collider.time()
            + elapsed_seconds.min(self.step_count as f64 * self.config.simulation_time_step);
        let mut next_time;
        loop {
            next_time = self.collider.next_time();
            if collider_time >= next_time {
                while let Some((HbEvent::Collide, p1, p2)) = self.collider.next() {
                    self.collision(p1, p2)
                }
                self.collider.set_time(next_time);
                self.last_collider_time = next_time;
                self.previous_time = now;
                self.step_count = 0;
            } else {
                break;
            }
        }
        if next_time < f64::INFINITY {
            self.step_count += 1;
        }
        collider_time - self.last_collider_time
    }

    fn add_ballast(&mut self, position: Vec2, velocity: Vec2, mass: f64) {
        self.add_particle(position, velocity, ParticleKind::Ballast, mass);
    }

    fn add_fuel(&mut self, position: Vec2, velocity: Vec2) {
        self.add_particle(
            position,
            velocity,
            ParticleKind::Fuel,
            self.config.fuel_mass,
        );
    }

    fn add_neutron(&mut self, position: Vec2, velocity: Vec2) {
        self.add_particle(
            position,
            velocity,
            ParticleKind::Neutron,
            self.config.neutron_mass,
        );
    }

    fn add_particle(&mut self, position: Vec2, velocity: Vec2, kind: ParticleKind, mass: f64) {
        let id = self.next_id();
        let profile = EntityProfile::new(id);
        let diameter = mass.sqrt() * self.config.particle_diameter_factor;
        let hitbox = Shape::circle(diameter).place(position).moving(velocity);
        self.collider.add_hitbox(profile, hitbox);
        let particle = Particle {
            kind,
            mass,
            neutrons_absorbed: 0,
        };
        self.particles.insert(id, particle);
    }

    fn remove_particle(&mut self, particle_id: HbId) {
        self.collider.remove_hitbox(particle_id);
        self.particles.remove(&particle_id);
    }

    fn collision(&mut self, p1: EntityProfile, p2: EntityProfile) {
        if let Some(collision) = self.detect_wall_collision(p1.id, p2.id) {
            self.wall_collision_elastic(collision)
        } else {
            self.particle_collision(p1, p2);
        }
    }

    fn wall_collision_elastic(&mut self, collision: WallCollision) {
        // Particular value doesn't matter, as long as it's positive
        const CENTER_DIFF_VALUE: f64 = 1.0;
        const_assert!(CENTER_DIFF_VALUE > 0.0);

        let object_hitbox = self.collider.get_hitbox(collision.object_id);
        let wall_id = self.wall_kind_to_id[&collision.wall];
        let wall_hitbox = self.collider.get_hitbox(wall_id);
        let v1 = object_hitbox.vel.value;
        let mut v1_new = object_hitbox.vel;
        let center_diff = match collision.wall {
            WallKind::Top => v2(0.0, CENTER_DIFF_VALUE),
            WallKind::Bottom => v2(0.0, -CENTER_DIFF_VALUE),
            WallKind::Left => v2(CENTER_DIFF_VALUE, 0.0),
            WallKind::Right => v2(-CENTER_DIFF_VALUE, 0.0),
        };

        let v2 = wall_hitbox.vel.value;
        v1_new.value = v1
            - (v1 - v2).scalar_product(center_diff)
                * (center_diff)
                * (1.0 / center_diff.len().powi(2))
                * 2.0;

        self.collider.set_hitbox_vel(collision.object_id, v1_new);
    }

    fn particle_collision(&mut self, p1: EntityProfile, p2: EntityProfile) {
        if let Some(CollisionMatch {
            matching: neutron,
            other,
        }) = self.match_collision(p1.id, p2.id, ParticleKind::Neutron)
        {
            match self.particles[&other].kind {
                ParticleKind::Neutron => self.particle_collision_elastic(p1, p2),

                ParticleKind::Fuel => {
                    self.absorb_neutron(neutron, other);
                    self.fission(other);
                }

                ParticleKind::Ballast => {
                    if self.particles[&other].neutrons_absorbed < self.config.max_neutrons_absorbed
                    {
                        self.absorb_neutron(neutron, other);
                    } else {
                        self.particle_collision_elastic(p1, p2);
                    }
                }
            }
        } else {
            self.particle_collision_elastic(p1, p2);
        }
    }

    fn particle_collision_elastic(&mut self, p1: EntityProfile, p2: EntityProfile) {
        let hb1 = self.collider.get_hitbox(p1.id);
        let hb2 = self.collider.get_hitbox(p2.id);
        let v1 = hb1.vel.value;
        let mut v1_new = hb1.vel;
        let v2 = hb2.vel.value;
        let mut v2_new = hb2.vel;
        let c1 = hb1.value.pos;
        let c2 = hb2.value.pos;
        let m1 = self.particles[&p1.id].mass;
        let m2 = self.particles[&p2.id].mass;
        v1_new.value = v1
            - ((c1 - c2) * (v1 - v2).scalar_product(c1 - c2) * 2.0 * m2)
                * (1.0 / ((m1 + m2) * (c1 - c2).len().powi(2)));
        v2_new.value = v2
            - ((c2 - c1) * (v2 - v1).scalar_product(c2 - c1) * 2.0 * m1)
                * (1.0 / ((m1 + m2) * (c2 - c1).len().powi(2)));
        self.collider.set_hitbox_vel(p1.id, v1_new);
        self.collider.set_hitbox_vel(p2.id, v2_new);
    }

    fn detect_wall_collision(&self, p1: HbId, p2: HbId) -> Option<WallCollision> {
        let wall1 = self.wall_id_to_kind.get(&p1);
        let wall2 = self.wall_id_to_kind.get(&p2);
        match (wall1, wall2) {
            (None, None) => None,
            (Some(&wall), None) => Some(WallCollision {
                wall,
                object_id: p2,
            }),
            (None, Some(&wall)) => Some(WallCollision {
                wall,
                object_id: p1,
            }),
            (Some(_), Some(_)) => panic!("wall-wall collision between {} and {}", p1, p2),
        }
    }

    fn match_collision(
        &self,
        p1: HbId,
        p2: HbId,
        either_kind: ParticleKind,
    ) -> Option<CollisionMatch> {
        if self.particles[&p1].kind == either_kind {
            Some(CollisionMatch {
                matching: p1,
                other: p2,
            })
        } else if self.particles[&p2].kind == either_kind {
            Some(CollisionMatch {
                matching: p2,
                other: p1,
            })
        } else {
            None
        }
    }

    fn absorb_neutron(&mut self, neutron_id: HbId, particle_id: HbId) {
        let m1 = self.particles[&neutron_id].mass;
        let v1 = self.collider.get_hitbox(neutron_id).vel.value;
        self.collider.remove_hitbox(neutron_id);
        self.particles.remove(&neutron_id);

        let particle = self.particles.get_mut(&particle_id).unwrap();
        let m2 = particle.mass;
        let mut particle_velocity = self.collider.get_hitbox(particle_id).vel;
        let v2 = particle_velocity.value;
        let total_mass = m1 + m2;
        particle_velocity.value = (v1 * m1 + v2 * m2) * (1.0 / total_mass);
        self.collider.set_hitbox_vel(particle_id, particle_velocity);
        particle.mass = total_mass;
        particle.neutrons_absorbed += 1;
    }

    fn fission(&mut self, particle: HbId) {
        let total_mass = self.particles[&particle].mass;
        let hitbox = self.collider.get_hitbox(particle);
        let position = hitbox.value.pos;
        let velocity = hitbox.vel.value;

        // Generate neutrons
        let neutrons_total_energy =
            self.config.fission_energy * self.config.neutron_energy_coefficient;
        let neutrons_total_mass =
            self.config.neutron_mass * self.config.neutrons_per_fission as f64;
        let neutrons = split(
            self.config.neutrons_per_fission,
            neutrons_total_energy,
            neutrons_total_mass,
        );
        for neutron in &neutrons {
            self.add_neutron(position, neutron.velocity);
        }

        // Generate fission products
        let products_total_energy = self.config.fission_energy - neutrons_total_energy;
        let products_total_mass = total_mass - neutrons_total_mass;
        let products = split(
            self.config.products_per_fission,
            products_total_energy,
            products_total_mass,
        );
        let original_momentum_contribution =
            velocity * (total_mass / self.config.products_per_fission as f64);
        for product in &products {
            let original_velocity_contribution =
                original_momentum_contribution * (1.0 / product.mass);
            self.add_ballast(
                position,
                product.velocity + original_velocity_contribution,
                product.mass,
            );
        }

        self.remove_particle(particle);
        self.fission_count += 1;
    }
}

fn random_velocity() -> Vec2 {
    let mut rng = rand::thread_rng();
    let mut gen_non_zero = || rng.gen_range(0.01, 1.0) * if rng.gen_bool(0.5) { 1.0 } else { -1.0 };
    let x = gen_non_zero();
    let y = gen_non_zero();
    v2(x, y).normalize().unwrap()
}

#[derive(Copy, Clone)]
struct SplitProduct {
    mass: f64,
    velocity: Vec2,
}

fn split(n: usize, total_energy: f64, total_mass: f64) -> Vec<SplitProduct> {
    let particle_energy = total_energy / n as f64;
    let particle_mass = total_mass / n as f64;
    let particle_speed = KineticEnergyToSpeed {
        energy: particle_energy,
        mass: particle_mass,
    }
    .result();
    let mut products = Vec::with_capacity(n);
    products.resize(
        n,
        SplitProduct {
            mass: particle_mass,
            velocity: Default::default(),
        },
    );
    for product in &mut products[0..n - 1] {
        product.velocity = random_velocity();
    }
    products.last_mut().unwrap().velocity = products[0..products.len() - 1]
        .iter()
        .fold(v2(0.0, 0.0), |velocity, product| {
            velocity - product.velocity
        });
    for product in &mut products {
        product.velocity *= particle_speed;
    }
    products
}

struct KineticEnergyToSpeed {
    energy: f64,
    mass: f64,
}

impl KineticEnergyToSpeed {
    fn result(self) -> f64 {
        (self.energy * 2.0 / self.mass).sqrt()
    }
}

struct WallCollision {
    wall: WallKind,
    object_id: HbId,
}

struct CollisionMatch {
    matching: HbId,
    other: HbId,
}

struct Particle {
    kind: ParticleKind,
    mass: f64,
    neutrons_absorbed: usize,
}

impl Index<HbId> for Reactor<'_> {
    type Output = Particle;

    fn index(&self, index: HbId) -> &Self::Output {
        &self.particles[&index]
    }
}

#[derive(Hash, Eq, PartialEq, Copy, Clone, Debug, IntoEnumIterator, Ord, PartialOrd)]
enum ParticleKind {
    Neutron,
    Fuel,
    Ballast,
}

#[derive(Copy, Clone, Debug)]
struct EntityProfile {
    id: HbId,
}

impl EntityProfile {
    fn new(id: HbId) -> Self {
        Self { id }
    }
}

impl HbProfile for EntityProfile {
    fn id(&self) -> u64 {
        self.id
    }

    fn can_interact(&self, _: &Self) -> bool {
        true
    }
}

#[derive(Copy, Clone, Debug, IntoEnumIterator, Eq, PartialEq, Hash)]
enum WallKind {
    Top,
    Bottom,
    Left,
    Right,
}

trait Vec2Ext {
    fn scalar_product(&self, other: Self) -> f64;
    fn div(&self, n: f64) -> Self;
}

impl Vec2Ext for Vec2 {
    fn scalar_product(&self, other: Self) -> f64 {
        self.x * other.x + self.y * other.y
    }

    fn div(&self, n: f64) -> Self {
        v2(self.x / n, self.y / n)
    }
}
